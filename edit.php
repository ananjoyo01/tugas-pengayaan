<?php include('config.php'); ?>


	<div class="container" style="margin-top:20px">
		<center><font size="6">Edit Data</font></center>

		<hr>

		<?php
		//jika sudah mendapatkan parameter GET id dari URL
		if(isset($_GET['id_dosen'])){
			//membuat variabel $id untuk menyimpan id dari GET id di URL
			$id_dosen = $_GET['id_dosen'];

			//query ke database SELECT tabel dosen berdasarkan id = $id
			$select = mysqli_query($koneksi, "SELECT * FROM dosen WHERE id_dosen='$id_dosen'") or die(mysqli_error($koneksi));

			//jika hasil query = 0 maka muncul pesan error
			if(mysqli_num_rows($select) == 0){
				echo '<div class="alert alert-warning">ID tidak ada dalam database.</div>';
				exit();
			//jika hasil query > 0
			}else{
				//membuat variabel $data dan menyimpan data row dari query
				$data = mysqli_fetch_assoc($select);
			}
		}
		?>

		<?php
		//jika tombol simpan di tekan/klik
		if(isset($_POST['submit'])){
			$id_dosen		= $_POST['id_dosen'];
			$foto_dosen		= $_POST['foto_dosen'];
			$nip_dosen		= $_POST['nip_dosen'];
			$nama_dosen		= $_POST['nama_dosen'];
			$fakultas		= $_POST['fakultas'];
			$prodi			= $_POST['prodi'];

			$sql = mysqli_query($koneksi, "UPDATE dosen SET id_dosen='$id_dosen', foto_dosen='$foto_dosen', nip_dosen='$nip_dosen', nama_dosen='$nama_dosen', fakultas='$fakultas', prodi='$prodi' WHERE id_dosen='$id_dosen'") or die(mysqli_error($koneksi));

			if($sql){
				echo '<script>alert("Berhasil menyimpan data."); document.location="index.php?page=tampil_dos";</script>';
			}else{
				echo '<div class="alert alert-warning">Gagal melakukan proses edit data.</div>';
			}
		}
		?>

		<form action="index.php?page=edit_dos&id_dosen=<?php echo $id_dosen; ?>" method="post">
			<div class="item form-group">
				<label class="col-form-label col-md-3 col-sm-3 label-align">Id_dosen</label>
				<div class="col-md-8 col-sm-8">
					<input type="text" name="id_dosen" class="form-control" size="4" value="<?php echo $data['id_dosen']; ?>" readonly required>
				</div>
			</div>
			<div class="item form-group">
				<label class="col-form-label col-md-3 col-sm-3 label-align">Foto dosen</label>
				<div class="col-md-8 col-sm-8">
					<input type="text" name="foto_dosen" class="form-control" value="<?php echo $data['foto_dosen']; ?>" required>
				</div>
			</div>
			<div class="item form-group">
				<label class="col-form-label col-md-3 col-sm-3 label-align">Nip dosen</label>
				<div class="col-md-8 col-sm-8">
					<input type="text" name="nip_dosen" class="form-control" value="<?php echo $data['nip_dosen']; ?>" required>
				</div>
			</div>
			<div class="item form-group">
				<label class="col-form-label col-md-3 col-sm-3 label-align">Nama dosen</label>
				<div class="col-md-8 col-sm-8">
					<input type="text" name="nama_dosen" class="form-control" value="<?php echo $data['nama_dosen']; ?>" required>
				</div>
			</div>
			<div class="item form-group">
				<label class="col-form-label col-md-3 col-sm-3 label-align">Prodi</label>
				<div class="col-md-8 col-sm-8">
				<input type="text" name="prodi" class="form-control" value="<?php echo $data['prodi']; ?>" required>
					<!-- <select name="prodi" class="form-control" required>
						<option value="">Pilih Prodi</option>
						<option value="Teknik Informatika" <?php if($data['prodi'] == 'Teknik Informatika'){ echo 'selected'; } ?>>Teknik Informatika</option>
						<option value="Teknik Sipil" <?php if($data['prodi'] == 'Teknik Sipil'){ echo 'selected'; } ?>>Teknik Sipil</option>
						<option value="Teknik Kimia" <?php if($data['prodi'] == 'Teknik Kimia'){ echo 'selected'; } ?>>Teknik Kimia</option>
						<option value="Teknik Elektro" <?php if($data['prodi'] == 'Teknik Elektro'){ echo 'selected'; } ?>>Teknik Elektro</option>
						<option value="Akuntansi" <?php if($data['prodi'] == 'Akuntansi'){ echo 'selected'; } ?>>Akuntansi</option>
						<option value="Manajemen" <?php if($data['prodi'] == 'Manajemen'){ echo 'selected'; } ?>>Manajemen</option>
						<option value="Farmasi" <?php if($data['prodi'] == 'Farmasi'){ echo 'selected'; } ?>>Farmasi</option>
						<option value="Hukum" <?php if($data['prodi'] == 'Hukum'){ echo 'selected'; } ?>>Hukum</option>
						<option value="Kedokteran" <?php if($data['prodi'] == 'Kedokteran'){ echo 'selected'; } ?>>Kedokteran</option>
					</select> -->
				</div>
			</div>
			<div class="item form-group">
				<div class="col-md-6 col-sm-6 offset-md-3">
					<input type="submit" name="submit" class="btn btn-primary" value="Simpan">
					<a href="index.php?page=tampil_dos" class="btn btn-warning">Kembali</a>
				</div>
			</div>
		</form>
	</div>
